
export const typeDefaults = {
  [typeof String()]: '',
  [typeof Number()]: null,
  [typeof Boolean()]: false
}

export function typeSorters (type, asc, by) {
  return {
    [typeof String()]: (a, b) => asc ? a[by].localeCompare(b[by]) : b[by].localeCompare(a[by]),
    [typeof Number()]: (a, b) => asc ? ((parseInt(a[by], 10) || 0) > (parseInt(b[by], 10) || 0) ? 1 : -1) : ((parseInt(a[by], 10) || 0) > (parseInt(b[by], 10) || 0) ? -1 : 1),
    [typeof Boolean()]: (a, b) => ((a[by] && b[by]) || (!a[by] && !b[by])) ? 0 : asc ? (b[by] && !a[by] ? 1 : -1) : (a[by] && !b[by] ? 1 : -1)
  }[type]
}
