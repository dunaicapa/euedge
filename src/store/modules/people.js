import { typeDefaults, typeSorters } from '@/common.js'

const peopleMock = require('./people.mock.json')

export const columns = {
  name: {
    title: 'Name',
    type: typeof String()
  },
  job: {
    title: 'Job title',
    type: typeof String()
  },
  age: {
    title: 'Age',
    type: typeof Number()
  },
  nick: {
    title: 'Nick',
    type: typeof String()
  },
  employee: {
    title: 'Employee',
    type: typeof Boolean()
  }
}

export const dummyPerson = Object.keys(columns).reduce((accu, key) => ({ ...accu, [key]: typeDefaults[columns[key].type] }), {})

const MUTATIONS = {
  ADD: 'ADD',
  REMOVE: 'REMOVE',
  SORT: 'SORT'
}

export const ACTIONS = {
  ADD: 'ADD',
  REMOVE: 'REMOVE',
  SORT: 'SORT'
}

export const GETTERS = {
  AGE_RANGE: 'AGE_RANGE'
}

export default {
  namespaced: true,
  state: {
    sort: {
      by: null,
      asc: true
    },
    all: peopleMock
  },
  mutations: {
    [MUTATIONS.ADD] (state, person) {
      state.all.push(person)
    },
    [MUTATIONS.REMOVE] (state, target) {
      state.all.splice(target, 1)
    },
    [MUTATIONS.SORT] (state, by) {
      state.sort.asc = (state.sort.by === by) ? !state.sort.asc : true
      state.sort.by = by
      state.all.sort(typeSorters(columns[by].type, state.sort.asc, state.sort.by))
    }
  },
  actions: {
    [ACTIONS.ADD] ({ commit }, person) {
      commit(MUTATIONS.ADD, person)
    },
    [ACTIONS.SORT] ({ commit }, by) {
      commit(MUTATIONS.SORT, by)
    },
    [ACTIONS.REMOVE] ({ commit }, target) {
      commit(MUTATIONS.REMOVE, target)
    }
  },
  getters: {
    /**
     * Returns an object collecting the distribution of all people in 10 year range
     * Key is the ceiled age range, Value is the range count
     * Invalid data stored under the -1 key
     * @param state
     * @returns {*}
     */
    [GETTERS.AGE_RANGE] (state) {
      return state.all.reduce((accu, current) => {
        let range = !isNaN(current.age) ? (Math.ceil(current.age / 10) * 10) : -1
        accu[range] = (typeof accu[range] === typeof undefined) ? 1 : (accu[range] + 1)
        return accu
      }, {})
    }
  }
}
